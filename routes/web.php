<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $user = \App\Models\User::create(
        [
            'name' => 'ashkan',
            'email' => 'as@gmail.com',
            'email_verified_at' => \Illuminate\Support\Carbon::now(),
            'password' => \Illuminate\Support\Facades\Hash::make('123'),
        ]
    );

    auth()->login($user);
    return view('welcome');
});


Route::resource('products', ProductController::class);

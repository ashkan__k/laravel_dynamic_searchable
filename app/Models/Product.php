<?php

namespace App\Models;

use App\Http\traits\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'title',
        'text',
        'is_active',
        'image',
    ];

    protected $search_fields = [
        'title',
        'text',
        'user.name',
        'user.email',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
